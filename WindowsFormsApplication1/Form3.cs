﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form3 : Form
    {
        bool autenticado = false;

        public bool Autenticado
        {
            get { return autenticado; }
            set { autenticado = value; }
        }
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == "prueba5")
            {
                autenticado = true;
            }
            else
            {
                autenticado = false;
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            autenticado = false;
            this.Close();
        }
    }
}
