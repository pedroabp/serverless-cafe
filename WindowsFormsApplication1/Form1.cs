﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // this.Deactivate += new EventHandler(Form1_Leave);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Leave(object sender, EventArgs e)
        {
            this.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F4)
            {
                e.Handled = true;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            this.label1.Location = new Point((this.Width - this.label1.Width) / 2, 10);
            this.button2.Location = new Point((this.Width - this.button2.Width - this.button3.Width - 50) / 2, 200);
            this.button3.Location = new Point(((this.Width - this.button2.Width - this.button3.Width - 50) / 2) + this.button2.Width + 50, 200);
            this.button4.Location = new Point((this.Width - this.button4.Width - this.button5.Width - 50) / 2, 300);
            this.button5.Location = new Point(((this.Width - this.button4.Width - this.button5.Width - 50) / 2) + this.button2.Width + 50, 300);
            this.button6.Location = new Point((this.Width - this.button6.Width - this.button7.Width - 50) / 2, 400);
            this.button7.Location = new Point(((this.Width - this.button6.Width - this.button7.Width - 50) / 2) + this.button2.Width + 50, 400);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // this.Hide();
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                this.Hide();
                Form2 form2 = new Form2();
                form2.ModoTemporizador = false;
                form2.Padre = this;
                form2.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                this.Hide();
                Form2 form2 = new Form2();
                form2.ModoTemporizador = true;
                form2.LimiteMinutos = 15;
                form2.LimiteHoras = 0;
                form2.Padre = this;
                form2.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                this.Hide();
                Form2 form2 = new Form2();
                form2.ModoTemporizador = true;
                form2.LimiteMinutos = 30;
                form2.LimiteHoras = 0;
                form2.Padre = this;
                form2.Show();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                this.Hide();
                Form2 form2 = new Form2();
                form2.ModoTemporizador = true;
                form2.LimiteMinutos = 45;
                form2.LimiteHoras = 0;
                form2.Padre = this;
                form2.Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                this.Hide();
                Form2 form2 = new Form2();
                form2.ModoTemporizador = true;
                form2.LimiteMinutos = 0;
                form2.LimiteHoras = 1;
                form2.Padre = this;
                form2.Show();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                Form4 form4 = new Form4();
                form4.ShowDialog();
                if (form4.Minutos != -1 && form4.Horas != -1)
                {
                    this.Hide();
                    Form2 form2 = new Form2();
                    form2.ModoTemporizador = true;
                    form2.LimiteMinutos = form4.Minutos;
                    form2.LimiteHoras = form4.Horas;
                    form2.Padre = this;
                    form2.Show();
                }
            }
        }
    }
}
