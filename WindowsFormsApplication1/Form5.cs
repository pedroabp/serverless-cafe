﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form5 : Form
    {
        int total = 0;
        public int Total
        {
            get { return total; }
            set
            {
                total = value;
                this.label2.Text = "$"+total.ToString();
            }
        }
        bool pagar = false;
        public bool Pagar
        {
            get { return pagar; }
            set { pagar = value; }
        }
        bool tiempoAdicional = false;
        public bool TiempoAdicional
        {
            get { return tiempoAdicional; }
            set { tiempoAdicional = value; }
        }
        Form2 padre;
        public Form2 Padre
        {
            get { return padre; }
            set { padre = value; }
        }
        public Form5()
        {
            InitializeComponent();
        }
        private void Form5_Load(object sender, EventArgs e)
        {

        }
        private void Form5_Resize(object sender, EventArgs e)
        {
            this.label1.Location = new Point((this.Width - this.label1.Width)/2,10);
            this.label2.Location = new Point(((this.Width - this.label2.Width - this.label3.Width - 20) / 2) + this.label3.Width + 20, 100);
            this.label3.Location = new Point((this.Width - this.label2.Width - this.label3.Width - 20) / 2, 100);
            this.button1.Location = new Point((this.Width - this.button1.Width) / 2, 200);
            this.button2.Location = new Point((this.Width - this.button2.Width) / 2, 300);
            this.button3.Location = new Point((this.Width - this.button3.Width) / 2, 400);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                pagar = false;
                tiempoAdicional = true;
                Form4 form4 = new Form4();
                form4.ShowDialog();
                if (form4.Minutos != -1 && form4.Horas != -1)
                {
                    padre.ModoTemporizador = true;
                    padre.LimiteMinutos = form4.Minutos;
                    padre.LimiteHoras = form4.Horas;
                    this.Dispose();
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();
            if (form3.Autenticado)
            {
                pagar = true;
                tiempoAdicional = false;
                this.Dispose();
            }
        }
        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F4)
            {
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
