﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form4 : Form
    {
        int minutos = -1;
        public int Minutos
        {
            get { return minutos; }
            set { minutos = value; }
        }
        int horas = -1;
        public int Horas
        {
            get { return horas; }
            set { horas = value; }
        }
        public Form4()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            minutos = Convert.ToInt32(this.numericUpDown1.Value);
            horas = Convert.ToInt32(this.numericUpDown2.Value);
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
