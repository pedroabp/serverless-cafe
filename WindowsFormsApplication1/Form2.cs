﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        bool modoTemporizador = true;
        public bool ModoTemporizador
        {
            get { return modoTemporizador; }
            set { modoTemporizador = value; }
        }
        int segundos = 0;
        public int Segundos
        {
            get { return segundos; }
            set
            {
                segundos = value;
                if (modoTemporizador)
                {
                    int segundosRestantes = 0 - segundos;
                    int minutosRestantes = limiteMinutos - minutos;
                    int horasRestantes = limiteHoras - horas;
                    if(segundosRestantes<0) {
                        minutosRestantes--;
                        segundosRestantes += 60;
                    }
                    if(minutosRestantes<0) {
                        horasRestantes--;
                        minutosRestantes += 60;
                    }
                    this.label5.Text = segundosRestantes.ToString();
                }
                else
                {
                    this.label5.Text = segundos.ToString();
                }
                if (this.label5.Text.Length < 2)
                {
                    this.label5.Text = "0" + this.label5.Text;
                }
            }
        }
        int minutos = 0;
        public int Minutos
        {
            get { return minutos; }
            set
            {
                minutos = value;
                if (modoTemporizador)
                {
                    int segundosRestantes = 0 - segundos;
                    int minutosRestantes = limiteMinutos - minutos;
                    int horasRestantes = limiteHoras - horas;
                    if (segundosRestantes < 0)
                    {
                        minutosRestantes--;
                        segundosRestantes += 60;
                    }
                    if (minutosRestantes < 0)
                    {
                        horasRestantes--;
                        minutosRestantes += 60;
                    }
                    this.label4.Text = minutosRestantes.ToString();
                }
                else
                {
                    this.label4.Text = minutos.ToString();
                }
                if (this.label4.Text.Length < 2)
                {
                    this.label4.Text = "0" + this.label4.Text;
                }
            }
        }
        int limiteMinutos = 0;
        public int LimiteMinutos
        {
            get { return limiteMinutos; }
            set { limiteMinutos = value; }
        }
        int horas = 0;
        public int Horas
        {
            get { return horas; }
            set
            {
                horas = value;
                if (modoTemporizador)
                {
                    int segundosRestantes = 0 - segundos;
                    int minutosRestantes = limiteMinutos - minutos;
                    int horasRestantes = limiteHoras - horas;
                    if (segundosRestantes < 0)
                    {
                        minutosRestantes--;
                        segundosRestantes += 60;
                    }
                    if (minutosRestantes < 0)
                    {
                        horasRestantes--;
                        minutosRestantes += 60;
                    }
                    this.label1.Text = horasRestantes.ToString();
                }
                else
                {
                    this.label1.Text = horas.ToString();
                }
                if (this.label1.Text.Length < 2)
                {
                    this.label1.Text = "0" + this.label1.Text;
                }
            }
        }
        int limiteHoras = 0;
        public int LimiteHoras
        {
            get { return limiteHoras; }
            set { limiteHoras = value; }
        }
        Form1 padre = null;
        public Form1 Padre
        {
            get { return padre; }
            set { padre = value; }
        }
        int totalAnterior = 0;
        public Form2()
        {
            InitializeComponent();
            this.Segundos = 0;
            this.Minutos = 0;
            this.Horas = 0;
        }
        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            segundos++;
            if (segundos == 60)
            {
                segundos = 0;
                minutos++;
            }
            if (minutos == 60)
            {
                minutos = 0;
                horas++;
            }
            int segundosRestantes = 0 - segundos;
            int minutosRestantes = limiteMinutos - minutos;
            int horasRestantes = limiteHoras - horas;
            if (segundosRestantes < 0)
            {
                minutosRestantes--;
                segundosRestantes += 60;
            }
            if (minutosRestantes < 0)
            {
                horasRestantes--;
                minutosRestantes += 60;
            }
            if (this.modoTemporizador && segundosRestantes == 0 && minutosRestantes == 0 && horasRestantes == 0)
            {
                cobrar();
            }
            this.Segundos = segundos;
            this.Minutos = minutos;
            this.Horas = horas;
            this.Text = this.label1.Text + ":" + this.label4.Text + ":" + this.label5.Text;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            cobrar();
        }
        private void cobrar()
        {
            this.timer1.Stop();

            int tiempoMinutos = Convert.ToInt32(Math.Round((Convert.ToDouble(horas) * 60) + Convert.ToDouble(minutos) + (Convert.ToDouble(segundos) / 60), 0));

            int pago = Convert.ToInt32(Math.Round(Convert.ToDouble(tiempoMinutos) * 16.7, 0));

            int unidadesCincuenta = Convert.ToInt32(Math.Round(Convert.ToDouble(pago) / 50));

            int pagoFinal = (unidadesCincuenta * 50) + totalAnterior;

            Form5 form5 = new Form5();
            form5.Total = pagoFinal;
            form5.Padre = this;
            form5.ShowDialog();

            if (form5.Pagar)
            {
                padre.Show();
                this.Dispose();
            }
            else if (form5.TiempoAdicional)
            {
                totalAnterior = pagoFinal;
                reiniciar();
                this.Show();
            }
        }
        public void reiniciar()
        {
            this.Segundos = 0;
            this.Minutos = 0;
            this.Horas = 0;
            this.timer1.Start();
        }
    }
}
